FROM node:18-alpine as base

FROM base as dependencies
WORKDIR /app
COPY ./package*.json ./
RUN npm install

FROM base as build
WORKDIR /app
COPY --from=dependencies /app/node_modules ./node_modules
COPY . .
RUN npm run build

FROM alpine as runtime
COPY --from=build /app/reqloader /reqloader
ENTRYPOINT ["sleep", "infinity"]