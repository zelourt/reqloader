const request = require("request");

const argv = require("minimist")(process.argv.slice(2));

const link = argv.u;
const reqCount = Number(argv.c) || 1;
const asyncReq = Boolean(argv.a);

let arrReqT = [];
let errReqCount = 0;
const startScriptTime = Date.now();

const main = async () => {
  if (!link) {
    console.log(`Enter url like: -u "http://example.com"`);
    process.exit();
  }

  console.log(
    `Req to send: ${reqCount}. Async: ${asyncReq ? "true" : "false"}`
  );

  if (asyncReq)
    await Promise.allSettled(
      [...Array(reqCount).keys()].map(() => sendReq(link))
    );
  else
    for (let i = 0; i < reqCount; i++)
      await sendReq(link).finally(() => {
        process.stdout.write(`> non async req [${i + 1}]\r`);
      });
};

const sendReq = async (url) =>
  new Promise((resolve) => {
    const t1 = Date.now();

    request(url, { json: true }, (err, res, body) => {
      if (err) ++errReqCount;

      arrReqT.push(Date.now() - t1);
      resolve();
    });
  });

const getAvgT = () => arrReqT.reduce((a, b) => a + b, 0) / arrReqT.length;

main().finally(() => {
  console.log(
    `Avg t: ${getAvgT() / 1000}s. Max t: ${
      Math.max(...arrReqT) / 1000
    }s. Min t: ${Math.min(...arrReqT) / 1000}s`
  );
  console.log(
    `Requests: ${reqCount}. Success: ${
      reqCount - errReqCount
    }. Errors: ${errReqCount}`
  );
  console.log(`Script executed ${(Date.now() - startScriptTime) / 1000}s`);

  process.exit();
});
