# Request utill

## Local instalation

```bash
$ npm i
```

## Local execute

```bash
& node ./ -u "https://example.com" -a -c 10
```

## Build binary and execute
```bash
$ npm run build
$ ./reqloader -u "https://example.com" -a -c 10
```

## Docker instalation and execute
Build
```bash
$ docker-compose up --build -d
```

Run command
```bash
$ docker exec -t reqloader ./reqloader -u "https://example.com" -a -c 10
```

## Command options
Key | Description | Require | Default | Example value
--- | --- | --- | --- | ---
`-u` | Url to send request | true | | "https://example.com"
`-a` | Flag send async or await queue | false | false |
`-c` | Count requests | false | 1  | 100
